﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SwimCoach.Startup))]
namespace SwimCoach
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
